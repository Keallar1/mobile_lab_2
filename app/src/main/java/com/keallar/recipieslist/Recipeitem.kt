package com.keallar.recipieslist

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
data class RecipeItem @OptIn(ExperimentalSerializationApi::class) constructor(
    val name: String,
    val tagline: String,
    @JsonNames("first_brewed") val firstBrewed: String,
    val description: String,
    @JsonNames("image_url") val imageUrl: String,
    val abv: Float
)
