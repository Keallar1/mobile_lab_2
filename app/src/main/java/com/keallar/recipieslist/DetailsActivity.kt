package com.keallar.recipieslist

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.activity.ComponentActivity

class DetailsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val recipeDescription = intent.getStringExtra("description")
        val recipeImageUrl = intent.getStringExtra("image_url")
        val recipeAbv = intent.getStringExtra("abv")
        if (recipeDescription.isNullOrEmpty()) {
            throw IllegalArgumentException("No 'description' parameter passed")
        } else if (recipeImageUrl.isNullOrEmpty()) {
            throw IllegalArgumentException("No 'image_url' parameter passed")
        } else if (recipeAbv.isNullOrEmpty()) {
            throw IllegalArgumentException("No 'abv' parameter passed")
        }

        Log.d("DetailsActivityLog","onCreate: $recipeDescription")

        setContentView(R.layout.activity_details)

        val button: Button = findViewById(R.id.show_image)
        button.setOnClickListener { onBrowseClick(recipeImageUrl) }

        val descTextView: TextView = findViewById(R.id.description)
        descTextView.text = "Description:\n$recipeDescription"

        val abvTextView: TextView = findViewById(R.id.abv)
        abvTextView.text = "ABV: $recipeAbv"
    }

    private fun onBrowseClick(url: String) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }
}