package com.keallar.recipieslist

import android.content.Context
import android.util.Log
import kotlinx.serialization.json.Json
import java.net.URL

fun getRecipes(context: Context, page: String, per_page: String) : List<RecipeItem> {
    val url = context.getString(R.string.punkapi_url, page, per_page)
    return try {
        val data = URL(url).readText()
        val format = Json { ignoreUnknownKeys = true }
        val parsedData = format.decodeFromString<List<RecipeItem>>(data)
        parsedData
    } catch (e: Exception) {
        e.printStackTrace()
        Log.d("Error", "getRecipes Exception", e)
        val empty: List<RecipeItem> = emptyList()
        empty
    }
}