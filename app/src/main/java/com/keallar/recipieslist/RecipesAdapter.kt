package com.keallar.recipieslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecipesAdapter(
    private val recipes: List<RecipeItem>,
    private val onItemClicked: (item: RecipeItem) -> Unit
) : RecyclerView.Adapter<RecipeViewHolder>() {

    private fun onViewHolderClicked(position: Int) {
        onItemClicked(recipes[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recipe_item, parent, false)
        return RecipeViewHolder(view, this::onViewHolderClicked)
    }

    override fun getItemCount(): Int = recipes.size

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.nameView.text = recipes[position].name
        holder.descView.text = recipes[position].tagline
        holder.firstBrewedView.text = recipes[position].firstBrewed
    }
}

class RecipeViewHolder(
    itemView: View,
    private val onItemClicked: (position: Int) -> Unit
) : RecyclerView.ViewHolder(itemView) {
    val nameView: TextView
    val descView: TextView
    val firstBrewedView: TextView

    init {
        nameView = itemView.findViewById(R.id.recipe_name)
        descView = itemView.findViewById(R.id.tagline)
        firstBrewedView = itemView.findViewById(R.id.first_brewed)
        itemView.setOnClickListener { onItemClicked(bindingAdapterPosition) }
    }
}