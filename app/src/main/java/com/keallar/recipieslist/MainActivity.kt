package com.keallar.recipieslist

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.RecyclerView
import kotlin.concurrent.thread

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val list: RecyclerView = findViewById(R.id.list)

        thread {
            val data = getRecipes(this, "1", "20")
            Handler(Looper.getMainLooper()).post {
                val adapter = RecipesAdapter(data, this::onRecipeItemClicked)
                list.adapter = adapter
            }
        }
    }

    private fun onRecipeItemClicked(item: RecipeItem) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra("description", item.description)
        intent.putExtra("image_url", item.imageUrl)
        intent.putExtra("abv", item.abv.toString())
        startActivity(intent)
    }
}
